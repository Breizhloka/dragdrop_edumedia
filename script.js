/*
  Drag & Drop JavaScript par CASULLI Yann
  Fait le 29.09.20

  Consignes :
    - Considérons une fenêtre rectangulaire  de taille 1024px x 768px
    - Le centre de cette fenêtre est matérialisé par un "gros" point noir, fixe.
    - Considérons 3 disques de couleurs distinctes avec un contour noir.
    - Ces disques peuvent être déplacés dans la fenêtre par simple drag&drop.
    - A chaque déplacement  on demande d'afficher dans la fenêtre la couleur du disque le plus proche du centre.
    - Ce projet s'effectuera en HTML / CSS / JS en utilisant la balise CANVAS et l'API correspondante.

  Temps de réalisation global : ~8h
    - Temps de recherches : ~3h
    - Temps de réalisation : ~4h
    - Temps d'amélioration (version générique/réutilisable) : ~1h

  Difficultés rencontrées :
    - N'ayant jamais utilisé la balise CANVAS et son API, j'ai dû prendre un temps pour l'étudier
      avant de réaliser ce projet.
    - J'ai aussi fait des recherches pour certains calculs comme pour celui de la fonction mouseInShape().
    - Ainsi que sur la création d'un dictionnaire via l'objet Map de JavaScript.
*/

let canvas = document.querySelector('#canvas');
let ctx = canvas.getContext('2d');
let shapes = [
  {x:800, y:400, radius:60, color:'yellow'},
  {x:300, y:600, radius:60, color:'blue'},
  {x:100, y:100, radius:60, color:'red'},
];
let canvasW = 1024;
let canvasH = 768;

let dist = new Map();
let centerX = 512;
let centerY = 394;

let dragging = false;
let startX,startY;
let selectedShapeIndex;

canvas.width = canvasW;
canvas.height = canvasH;

//Dessin du centre
const drawCenter = () => {
  ctx.beginPath();
  ctx.lineWidth="15";
  ctx.arc(canvasW/2, canvasH/2, 10, 0, 2 * Math.PI, false);
  ctx.stroke();
  ctx.fillStyle = "black";
  ctx.fill();
};

//Event listener
canvas.onmousedown = mouseDown;
canvas.onmousemove = mouseMove;
canvas.onmouseup = mouseUp;
canvas.onmouseout = mouseOut;

// ------------------ CRÉATION DU CANVAS ET DES MÉCANISMES DRAG&DROP ------------------//
// ~2h de recherches / ~3h de réalisation

//On fait apparaître le canvas
draw();
document.getElementById("closerColor").innerHTML = closer();

//Création des cerles
function draw(){
  ctx.clearRect(0,0,canvasW,canvasH);
  for (var i = 0; i < shapes.length; i++) {
  let shape = shapes[i];
    ctx.beginPath();
    ctx.lineWidth="15";
    ctx.arc(shape.x, shape.y, shape.radius, 0, 2 * Math.PI, false);
    ctx.stroke();
    ctx.fillStyle = shape.color;
    ctx.fill();
    drawCenter();
  }
}

//On cherche à savoir si le curseur est dans un cercle
function mouseInShape(mx,my,shape){
  //On prend les distances entre les coordonnées du clique et celles du cercle
  var dx=mx-shape.x;
  var dy=my-shape.y;
  //Si les coordonnées du clique se situent dans l'aire du cercle, alors on retourne vrai
  if(dx*dx+dy*dy<shape.radius*shape.radius){
      return(true);
  }
}

//Écoute lorsque l'on clique dans un cercle, et le garde en mémoire
function mouseDown(e){
  //On prend les coordonnées du clique
  startX = e.offsetX;
  startY = e.offsetY;
  //On parcours les cercles
  for(var i=0; i<shapes.length; i++){
    //Si les coordonnées du clique correspondent aux coordonnées d'un cercle
    if(mouseInShape(startX,startY,shapes[i])){
      //Alors on mémorise l'index de ce cercle, on place un marqueur de déplacement, puis on arrête de parcourir les cercles
        selectedShapeIndex = i;
        dragging = true;
        return;
    }
  }
}

//Écoute lorsque l'on déplace le pointer
function mouseMove(e){
    // si on ne déplace aucun cercle, on arrête la fonction
    if(!dragging){return;}
    e.preventDefault();
    e.stopPropagation();
    // on calcule les coordonnées du pointer
    mouseX= e.offsetX;
    mouseY= e.offsetY;
    // on calcule la distance entre la position de départ et celle d'arrivée
    var dx=mouseX-startX;
    var dy=mouseY-startY;
    // on modifie les coordonnées du cercle selectionné par rapport à la distance parcourue
    var selectedShape=shapes[selectedShapeIndex];
    selectedShape.x+=dx;
    selectedShape.y+=dy;
    // on redessine le canvas
    draw();
    document.getElementById("closerColor").innerHTML = closer();
    // on met à jour les coordonnées du clique
    startX=mouseX;
    startY=mouseY;
}

function mouseUp(e){
    // si on ne déplace aucun cercle, on arrête la fonction
    if(!dragging){return;}
    e.preventDefault();
    e.stopPropagation();
    // on arrête le déplacement donc on remet le marqueur dans son état initial
    dragging=false;
}

function mouseOut(e){
    // si on ne déplace aucun cercle, on arrête la fonction
    if(!dragging){return;}
    e.preventDefault();
    e.stopPropagation();
    // on arrête le déplacement donc on remet le marqueur dans son état initial
    dragging=false;
}

// ------------------ RECHERCHE DU CERCLE LE PLUS PROCHE DU CENTRE ------------------//
// ~1h de recherches / ~1h de réalisation

//Calcul de la distance entre un cercle et le centre
function closer(){
  let closerColorValue;
  let closerColorKey;
  for (var i = 0; i < shapes.length; i++) {
    dist.set(shapes[i].color, Math.hypot((shapes[i].x - centerX), (shapes[i].y - centerY)));
  }

  for (let [key, value] of dist) {
    if (!closerColorValue) {
      closerColorValue = value;
      closerColorKey = key;
    } else if (closerColorValue > value) {
        closerColorValue = value;
        closerColorKey = key;

    }
  }
  return closerColorKey;
}
