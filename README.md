DragDrop_eduMedia
  Drag & Drop JavaScript par CASULLI Yann
  Fait le 29.09.20

  Consignes :
    - Considérons une fenêtre rectangulaire  de taille 1024px x 768px
    - Le centre de cette fenêtre est matérialisé par un "gros" point noir, fixe.
    - Considérons 3 disques de couleurs distinctes avec un contour noir.
    - Ces disques peuvent être déplacés dans la fenêtre par simple drag&drop.
    - A chaque déplacement  on demande d'afficher dans la fenêtre la couleur du disque le plus proche du centre.
    - Ce projet s'effectuera en HTML / CSS / JS en utilisant la balise CANVAS et l'API correspondante.

  Temps de réalisation global : ~8h
    - Temps de recherches : ~3h
    - Temps de réalisation : ~4h
    - Temps d'amélioration (version générique/réutilisable) : ~1h

  Difficultés rencontrées :
    - N'ayant jamais utilisé la balise CANVAS et son API, j'ai dû prendre un temps pour l'étudier
      avant de réaliser ce projet.
    - J'ai aussi fait des recherches pour certains calculs comme pour celui de la fonction mouseInShape().
    - Ainsi que sur la création d'un dictionnaire via l'objet Map de JavaScript.
